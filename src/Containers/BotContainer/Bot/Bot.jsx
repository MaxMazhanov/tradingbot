import React from 'react';

import './Bot.styl';

const bot = props => {
    const data = props.data;
    const bot = require(`./assets/${data.name}.png`);
    const value = data[props.curRange];
    const isPositive = value > 0;

    return (
        <div className={`Bot ${props.isActive ? 'active' : ''}`} onClick={props.onClickHandler}>
            <div className="botColumn">
                <img src={bot} alt="Bot icon" />
                <span className="boldText whiteText">{data.strategy.toUpperCase()}</span>
                <span className={`boldText ${isPositive ? 'positive' : 'negative'}`}>{isPositive ? '+' : ''}{value}%</span>
            </div>
        </div>
    );
}

export default bot;
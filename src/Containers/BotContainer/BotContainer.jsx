import React, { Component } from 'react';
import Bot from './Bot/Bot';

import './BotContainer.styl';

export default class BotContainer extends Component {
    state = {
        selectedBot: parseInt(this.getSelectedBot())
    }

    getSelectedBot() {
        let selectedBot = localStorage.getItem('selectedBot');
        return selectedBot = selectedBot || null;
    }

    onSelectBot(index) {
        this.setState({
            selectedBot: index
        })
        localStorage.setItem('selectedBot', index)
    }

    render() {
        return (
            <div className="BotContainer">
                {this.props.data && this.props.data.map((bot, index) => (
                    <Bot
                        key={index}
                        data={bot}
                        curRange={this.props.curRange}
                        onClickHandler={() => this.onSelectBot(index)}
                        isActive={index === this.state.selectedBot}
                    />
                ))}
            </div>
        );
    }
}
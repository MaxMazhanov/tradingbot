import React from 'react';

import graphImg from './assets/graph.png';
import './Graph.styl'

const graph = props => {
    return (
        <div className='Graph'>
            {/* Some cool graph building here! */}
            <img src={graphImg} alt="Graph" />
        </div>
    );
}

export default graph;
import React from 'react';

import './Header.styl'

const header = props => {
    const burgerMenuIcon = require('./assets/burger.png');
    const refreshIcon = require('./assets/refresh.png');
    return (
        <div className='Header'>
            <img src={burgerMenuIcon} alt="Menu" />
            <span className="boldText">{props.title}</span>
            <img src={refreshIcon} alt="Refresh" />
        </div>
    );
}

export default header;
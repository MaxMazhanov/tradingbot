import React from 'react';

import './FooterItem.styl';

const footerItem = props => {
    const icon = require(`./assets/${props.icon}.png`);
    const classes = `FooterItem ${props.isActive ? 'whiteText' : ''} ${props.notifications ? 'haveNotifications' : ''}`;
    return (
        <div className={classes}>
            <img src={icon} alt="Footer icon" />
            <div>{props.text}</div>
            {
                props.notifications ?
                    <div className="notifications boldText whiteText">{props.notifications}</div> :
                    null
            }
        </div>
    );
}

export default footerItem;
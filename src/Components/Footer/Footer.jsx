import React from 'react';
import FooterItem from './FooterItem/FooterItem';

import './Footer.styl'

const footer = props => {
    return (
        <div className='Footer'>
            <FooterItem icon="dash" text="Dashboard" isActive/>
            <FooterItem icon="megabot" text="Megabot" />
            <FooterItem icon="market" text="Bot market" />
            <FooterItem icon="prices" text="Coin prices" />
            <FooterItem icon="profile" text="Profile" notifications={3}/>
        </div>
    );
}

export default footer;
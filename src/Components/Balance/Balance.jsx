import React from 'react';

import coin from './assets/coin.png';
import './Balance.styl'

const balance = props => {
    const data = props.data;

    return (
        <div className='Balance'>
            <span className="boldText">TRADING CAPITAL</span>
            <div className="balanceContainer">
                <span className="capital whiteText">{`${data.capital} ${data.currency.toUpperCase()}`}</span>
                <div className="balance">
                    <span className="boldText">BALANCE:</span>
                    <span className="whiteText values">{data.balance}</span>
                    <img src={coin} alt="Coin" className="coinIcon" />
                    <span className="boldText">ON HOLD:</span>
                    <span className="whiteText values">{data.onHold}</span>
                    <img src={coin} alt="Coin" className="coinIcon" />
                </div>
            </div>
        </div>
    );
}

export default balance;
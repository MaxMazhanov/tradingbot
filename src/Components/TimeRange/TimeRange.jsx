import React from 'react';

import './TimeRange.styl'

const tabs = {
    '24h': '24h',
    '7d': '7 days',
    '30d': '30 days',
    'all_time': 'All time'
};

const timeRange = props => {
    return (
        <div className='TimeRange'>
            <span className="boldText">Time Range: </span>
            {Object.entries(tabs).map(([key, value], index) => (
                <div
                    className={`tab ${props.curRange === key ? 'tab_active' : ''}`}
                    key={index}
                    onClick={() => props.onRangeChangeHandler(key)}
                >
                    {value}
                </div>
            ))}
        </div>
    );
}

export default timeRange;
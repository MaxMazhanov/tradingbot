import React, { Component } from 'react';
import Main from '../Pages/Main/Main';

import './App.styl'

class App extends Component {
  render() {
    return (
      <div className='App'>
        <Main />
      </div>
    );
  }
}

export default App;

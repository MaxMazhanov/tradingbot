import React, { Component } from 'react';
import Header from '../../Components/Header/Header';
import Balance from '../../Components/Balance/Balance';
import Graph from '../../Components/Graph/Graph';
import BotContainer from '../../Containers/BotContainer/BotContainer';
import TimeRange from '../../Components/TimeRange/TimeRange';
import Footer from '../../Components/Footer/Footer';

import './Main.styl'

class Main extends Component {
  state = {
    data: {},
    curRange: this.getCurRange()
  }

  componentDidMount() {
    this.fetch();
  }

  fetch() {
    const data = require('../../../data/data.json');
    this.setState({
      data
    });
  }

  getCurRange() {
    return localStorage.getItem('curRange') || 'all_time';
  }

  onRangeChange(value) {
    this.setState({
      curRange: value
    });
    localStorage.setItem('curRange', value)
  }

  render() {
    const data = this.state.data;
    const balanceData = data && {
      capital: data.trading_capital || 0,
      currency: data.trading_capital_currency || '',
      balance: data.balance || 0,
      onHold: data.on_hold || 0
    };

    return (
      <div className='Main'>
        <Header title="Dashboard"/>
        <Balance data={balanceData}/>
        <Graph />
        <BotContainer data={data.bots} curRange={this.state.curRange}/>
        <TimeRange curRange={this.state.curRange} onRangeChangeHandler={this.onRangeChange.bind(this)}/>
        <Footer />
      </div>
    );
  }
}

export default Main;

## Start project

Для того, чтобы запустить проект необходимо проделать следующие действия:

1. Скачать проект
2. Установить node_modules `npm i`
3. Запустить проект `npm run dev`
4. Включить Google Chrome DevTools mobile emulator с необходимым девайсом/разрешением